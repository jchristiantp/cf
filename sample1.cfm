<cfinclude template="/includes/top.cfm">

<style type="text/css">
	.container{
		background-color:#FFFFFF;		
	}
	
	
	
	.row-container-first {
		background-color:#C6883D;
	}
	
	.row-container-first {
		color:#FFFFFF;		
	}
	
	.card {
		border-width:2px;
		/*border:none;
		border: 1px solid #e14040;
		border-radius: 0px;*/
		
	}
	
	.card-body{
		background-color: #C6883D;
		padding:1rem;
	}	
	
	.card-text{
		min-height: 90px;
	}
	
	.btn-primary{
		border: 1px solid #FFFFFF;
	}
</style>

<cfquery name="getWebcasts" datasource="#application.CMEWebcastSource#">
	SELECT      
				/*WEBCAST*/
				WC_WebcastID, 
				Title, 
				FileName, 
				Thumbnail, 
				
				/*PROJECT*/
				CME_Project_ID, 
				ProjectCode, 
				ProjectTopicDisplay, 
				Project_LinkURL,
				
				/*DISEASE STATE*/
				Name, 
				Description, 
				
				/*SPEAKER*/
				SpeakerID,
				SpeakerFirstName, SpeakerLastName, SpeakerSuffix

	FROM            WC_Webcasts_Details
	ORDER BY Name, SpeakerID
	
</cfquery>



<div class="container-fluid">
	
			<div class="row justify-content-center my-2">		
				<div class="col-12 col-sm-10">
					<h3 class="display-5">GHAPP FAQ Webcast Library</h3>			
				</div>
			</div>
			
			<cfset arrayOfColors = ["##C78F4B", "##092F57", "##829794"] >
			<cfset current_color = 0>
			
			<cfoutput query="getWebcasts" group="SpeakerID">				
				
				<div class="row justify-content-center row-container-first" style="background-color: #arrayOfColors[(current_color MOD ArrayLen(arrayOfColors)) + 1]#" >		
					
					<div class="col-12">
						<div class="row justify-content-center">				
							<div class="col-12 col-sm-10 py-5">
								<h3 class="display-5 text-center ">#Description#</h3>
								<h4 class="display-5 mb-3 text-center">Presented by #SpeakerFirstName# #SpeakerLastName#, #SpeakerSuffix#</h4>	
								
								<div class="row justify-content-center ">				
									<cfoutput>
										<div class="col-12 col-sm-4 d-flex mt-3 mt-sm-1 justify-content-center">
											<div class="card " style="width: 20rem;">
											  <img class="card-img-top" src="./Images/Speakers/#Thumbnail#" alt="Card image cap">
											  <div class="card-body" style="background-color: #arrayOfColors[(current_color MOD ArrayLen(arrayOfColors)) + 1]#">
												<h5 class="card-title">FAQ:</h5>
												<p class="card-text">#Title#</p>
												<a href="viewWebcast.cfm?webcastID=#WC_WebcastID#" class="btn btn-primary" target="_blank">View Now</a>
											  </div>
											</div>
										</div>							
									</cfoutput>
								</div>
								
								<cfset current_color = current_color + 1>
																
							</div>
						</div>
					</div>
				</div>
			
			</cfoutput>
				<div class="row justify-content-center row-container-first" >		
					<div class="col-12">
						<div class="row justify-content-center">				
							<div class="col-12 col-sm-10">
								<div class="row justify-content-center my-5">				
									<div class="col-12 col-sm-10 text-center">
										<div class="row justify-content-center ">				
											<div class="col-12 col-sm-4 text-center">																						
												<cfoutput><button class="btn btn-primary btn-block btn-lg btnClaimCredits p-3" type="button" data-id="#getWebcasts.CME_Project_ID#" >Claim Credits</button></cfoutput>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
</div>



<script type="text/javascript">
	$(document).ready(function() {
		$('.btnClaimCredits').on('click', function(){			
			var url;
			
			var thisID = $(this).attr('data-id')
			$.ajax({
				url:"WebcastEntry/cfcs/cmeproject.cfc?method=getProjectLink&returnformat=json",
				type:"POST",		
				dataType: "json",
				data: ({cmeProjectID : thisID}),
				success:function(result){					
					if (result["STATUS"] == 1 ){
						//url = result["KEYURL"];
						var url = result["KEYURL"];
						$.redirect( url, {cme_proj_id: thisID}, "POST", "_self");
					}else{
						bootbox.alert('Error. URL is not set for this project.');
					}					
				},
				error:function(){
					bootbox.alert('Error with this project.');
				}
			});
		})
	})
</script>

<cfinclude template="/includes/bottom.cfm">