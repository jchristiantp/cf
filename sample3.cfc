<cfcomponent output="no">
    
	<cffunction name="confirmArchive" access="private" returnType="boolean" returnFormat="plain"> 
		<cfargument name="meetingSeries" type="string" required="yes" hint="Program Series to be evaluated">
		
		<cfquery name="findUnfinishedMeetings" datasource="#application.PrimaryDataSource#">
			SELECT COUNT(*) AS MEETINGSFOUND
			FROM Meetings INNER JOIN
                         MeetingTypes ON Meetings.MeetingTypeID = MeetingTypes.MeetingTypeID
			WHERE MEETINGSTATUSID NOT IN (4, 6, 0, 8 ) AND MeetingTypes.MeetingIDPrefix = '#meetingSeries#'
		</cfquery>
		
		<cfif findUnfinishedMeetings.MEETINGSFOUND EQ 0>
			<!--- OK TO ARCHIVE --->
			<cfreturn 1>
		<cfelse>
			<!--- CAN'T ARCHIVE --->
			<cfreturn 0>
		</cfif>

	</cffunction>
	
    <cffunction name="updateProgramDetails" access="remote" returnType="string" returnFormat="plain"> 
		<cfargument name="formData" type="string" required="yes" hint="Form Data Serialize">
	
		<cftry>
			<cfset var formStruct = {} />
			<cfset formStruct = DeserializeJSON(arguments.formData)>
			
			<cfset returnMessage = "">
			
			<!--- HANDLING BOOTSTRATOGGLE VALUE SUBMITTED BY THE FORM --->
			<cfif ParameterExists(formStruct.ISTARGETAUDIENCEREPORT)>
				<cfset ISTARGETAUDIENCEREPORT = 1>
			<cfelse>
				<cfset ISTARGETAUDIENCEREPORT = 0>
			</cfif>
			
			<!--- HANDLING BOOTSTRATOGGLE VALUE SUBMITTED BY THE FORM --->
			<cfif ParameterExists(formStruct.ISSERIESSCHEDULEREPORT)>
				<cfset ISSERIESSCHEDULEREPORT = 1>
			<cfelse>
				<cfset ISSERIESSCHEDULEREPORT = 0>
			</cfif>
			
			<!--- HANDLING BOOTSTRATOGGLE VALUE SUBMITTED BY THE FORM --->
			<cfif ParameterExists(formStruct.ISARCHIVED)>
				<cfset isArchiveConfirm = confirmArchive(formStruct.MEETINGTYPEID)>
				<cfif isArchiveConfirm EQ 1>
					<cfset ISARCHIVED = 1>					
				<cfelse>				
					<cfset ISARCHIVED = 0>					
					<cfset returnMessage = " - Can't archive this program. Current meetings exist.">
				</cfif>
			<cfelse>
				<cfset ISARCHIVED = 0>
			</cfif>
			
			<cfif !ParameterExists(formStruct.SYSTEM_SENDFROMCONTACT)>
				<cfset SYSTEM_SENDFROMCONTACT = "">			
			<cfelse>
				<cfset SYSTEM_SENDFROMCONTACT = formStruct.SYSTEM_SENDFROMCONTACT>
			</cfif>

			<cfif !ParameterExists(formStruct.SYSTEM_SENDFROMEMAIL)>
				<cfset SYSTEM_SENDFROMEMAIL = "">			
			<cfelse>
				<cfset SYSTEM_SENDFROMEMAIL = formStruct.SYSTEM_SENDFROMEMAIL>			
			</cfif>

			<cfif !ParameterExists(formStruct.SYSTEM_MAILFROMCONTACT)>
				<cfset SYSTEM_MAILFROMCONTACT = "">			
			<cfelse>
				<cfset SYSTEM_MAILFROMCONTACT = formStruct.SYSTEM_MAILFROMCONTACT>		
			</cfif>

			
			<cfquery name="updateSeries" datasource="#application.PrimaryDataSource#">
				UPDATE MeetingTypes
				SET  OfficialTitle = '#formStruct.OFFICIALTITLE#', 
					 GrantNumberOfMeetings = #formStruct.GRANTNUMBEROFMEETINGS#, 
					 TargetAttendance = #formStruct.TARGETATTENDANCE#,
					 TargetAudienceSpecialty = '#formStruct.TARGETAUDIENCESPECIALTY#', 
					 TargetAudienceTitle = '#formStruct.TARGETAUDIENCETITLE#',
					 SpecialtyHighlight = '#formStruct.SPECIALTIESHIGHLIGHTED#',
					 ISTARGETAUDIENCEREPORT = #ISTARGETAUDIENCEREPORT#,
					 ISSERIESSCHEDULEREPORT = #ISSERIESSCHEDULEREPORT#,
					 ISARCHIVED = #ISARCHIVED#,
					 SYSTEM_SENDFROMCONTACT = <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(SYSTEM_SENDFROMCONTACT)#" null="#NOT len(trim(SYSTEM_SENDFROMCONTACT))#" />, 
					 SYSTEM_SENDFROMEMAIL = <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(SYSTEM_SENDFROMEMAIL)#" null="#NOT len(trim(SYSTEM_SENDFROMEMAIL))#" />, 
					 SYSTEM_MAILFROMCONTACT = <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(SYSTEM_MAILFROMCONTACT)#" null="#NOT len(trim(SYSTEM_MAILFROMCONTACT))#" />
				WHERE MeetingIDPrefix = '#formStruct.MEETINGTYPEID#'
			</cfquery>
				
					
			<cfreturn "Update successful" & returnMessage >      	
		
		<cfcatch type="any">
			<cfreturn cfcatch.detail & " " & cfcatch.message>
		</cfcatch>
			
		</cftry>		
		
    </cffunction> 
	
	<cffunction name="getProgramDetails" access="remote" returnType="any" returnFormat="jSON"> 
    	<cfargument name="programPrefix" type="string" required>
		
     	<cfquery name="selectSeriesDetails" datasource="#application.PrimaryDatasource#">
        	SELECT MeetingTypeDescription, TargetAudienceSpecialty, TargetAudienceTitle, OfficialTitle, ISNULL(TargetAttendance,0) AS TargetAttendance, 
                         ISNULL(GrantNumberOfMeetings,0) AS GrantNumberOfMeetings, ISTARGETAUDIENCEREPORT, SPECIALTYHIGHLIGHT, ISSERIESSCHEDULEREPORT, ISARCHIVED, 
						 SYSTEM_SENDFROMCONTACT, SYSTEM_SENDFROMEMAIL, SYSTEM_MAILFROMCONTACT
			FROM            MeetingTypes
			WHERE MeetingIDPrefix = '#programPrefix#'
        </cfquery>
        
		<cfset result = SerializeJSON(selectSeriesDetails, "struct")>
      	<cfreturn result/>
    </cffunction>	
        
</cfcomponent>