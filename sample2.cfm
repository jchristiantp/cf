<cfinclude template="/includes/top.cfm">

<style type="text/css">
	.container{
		background-color:#FFFFFF;
		margin-top:31px;
	}
	
</style>

<cfquery name="getWebcasts" datasource="#application.CMEWebcastSource#">
	SELECT        FileName, SpeakerFirstName, SpeakerLastName, SpeakerSuffix, Title
	FROM            WC_Webcasts_Details
	WHERE WC_WebcastID = #URL.webcastID#
</cfquery>
<main class="flex-fill">
<div class="container-fluid">
	<div class="row justify-content-center my-3">		
		<div class="embed-responsive embed-responsive-16by9 col-12 col-sm-6">
			<iframe
				  src="https://player.vimeo.com/video/<cfoutput>#getWebcasts.FileName#</cfoutput>"			  
				  scrolling="no"				  
				  allow="autoplay; fullscreen"
				   <!--- style="position:absolute;top:0;left:0;width:100%;height:100%;" --->
				  >
			</iframe>
		</div>
	</div>	
	<div class="row justify-content-center">				
		<div class="col-12 col-sm-6 ">
			<cfoutput>
			<div class="row ">	
				<div class="col-12 my-1 d-flex d-inline">					
					<p class="text-muted">#getWebcasts.Title#</p>
				</div>
			</div>
			<div class="row ">					
				<div class="col-12 col-sm-6 my-1 d-flex d-inline">						
					<h4> #getWebcasts.SpeakerFirstName# #getWebcasts.SpeakerLastName#, #getWebcasts.SpeakerSuffix#</h4>
				</div>
				<!--- <div class="col-12 col-sm-2 text-center my-2">										
					<button class="btn btn-primary btn-block" id="btnAccred" disabled>Accreditation</button>
				</div> --->
			</div>
			</cfoutput>
		</div>
	</div>
	
	<div class="row justify-content-center my-3">				
		<div class="col-12 col-sm-10 text-center">
			<div class="row justify-content-center ">				
				<div class="col-12 col-sm-2 text-center my-2">					
					<a class="btn btn-primary btn-block" href="./">Back to Webcasts</a>
				</div>
				<!--- <div class="col-12 col-sm-2 text-center my-2">										
					<button class="btn btn-primary btn-block" id="btnAccred" disabled>Accreditation</button>
				</div> --->
			</div>
		</div>
	</div>
</div>

<script src="https://player.vimeo.com/api/player.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var iframe = document.querySelector('iframe');
		var player = new Vimeo.Player(iframe);

		player.on('play', function() {
		  console.log('Played the video');		  
		});

		player.getVideoTitle().then(function(title) {
		  console.log('title:', title);
		});
		
		player.on('ended', function(data) {
		  // 'data' is an object containing properties specific to that event
		  	$('#btnAccred').prop('disabled', false);	  
		});
	})
</script>
</main>
<cfinclude template="/includes/bottom.cfm">